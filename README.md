# Level Up - Genetic Algorithms

## Links

- Source: [https://bitbucket.org/Coilz/level-up/src](https://bitbucket.org/Coilz/level-up/src)
- Presentation: [Level Up - Genetic Algorithms](https://sway.com/NqifWmeBTJqLEBuC?ref=Link)

## Project Setup
Clone this repository
```
$ git clone https://bitbucket.org/Coilz/level-up.git
```

To be able to use gulp in the command line, install [gulp-cli](https://github.com/gulpjs/gulp-cli):
```
$ npm install -g gulp-cli
```

This project uses [gulp](http://gulpjs.com/) as its build system and needs [typescript](https://www.typescriptlang.org/) to transpile the sources to javascript.

Install typescript & gulp
```
$ npm install -g typescript gulp
```

Install dependencies
```
$ npm install
```

Build and run
```
$ gulp
```

## License

MIT

## Exercises
The exercises and end results of excercises have been committed to various branches in this repo, so before you start, you should fetch the remote branches:
```
$ git fetch
```
### Exercise 1 - Changing GA parameters
For this exercise you can use the _master_ branch:
```
$ git checkout master
```

Fiddling with some options to see the consequences:

- change mutation rate
- change the mutate function in string-search

### Exercise 2 - Changing the fitness function
For this exercise you can use the _fitness_ branch:
```
$ git checkout fitness
```

The fitness function of the string-search can be changed to use a 'distance' for each character. Will this result in a faster optimization? Does some other GA function need to change?

### Exercise 3 - Use another selection method
For this exercise you can use the _selection_ branch:
```
$ git checkout selection
```

1. Implement a roulette wheel selection based on the fitness. Keep in mind that sometimes a fitness value can be negative.
2. Implement a tournament selection.

### Exercise 4 - Use another crossover method
For this exercise you can use the _uniformcrossover_ branch:
```
$ git checkout uniformcrossover
```

1. Implement a multi-point crossover method.
2. Implement a uniform crossover method.

Does any of the methods result in a faster GA?

### Exercise 5 - Use a steady state population
For this exercise you can use the _steadystate_ branch:
```
$ git checkout steadystate
```

Steady state GA generates one or two off-springs in each iteration and they replace one or two individuals from the population. A steady state GA is also known as _Incremental GA_. Implement the steady state in the algorithm.

### Exercise X - Write the GA for the Knapsack Problem
You can read about this problem [here](https://en.wikipedia.org/wiki/Knapsack_problem).

For this exercise you can use the _knapsack_ branch:
```
$ git checkout knapsack
```

Things to consider:

- representation of the genotype
    - mating
    - mutating
- representation of the phenotype
    - fitness function
- which selection
- terminating criterium
