import { Phenotype as BasePhenotype, IPhenotype } from "../genetic-algorithm/phenotype";
import { FitnessFunc } from "../genetic-algorithm/algorithm";
import { Journey } from "./journey";

export class PhenotypeBuilder {
    private readonly distanceFitness: FitnessFunc = (phenotype: IPhenotype) => {
        return 1 / (phenotype as Phenotype).getTotalDistance();
    }

    public build(value: Journey[]): Phenotype {
        return new Phenotype(value, this.distanceFitness);
    }
}

export class Phenotype extends BasePhenotype {
    private totalDistance = 0;
    constructor(private value: Journey[], fitnessFunc: FitnessFunc) {
        super(fitnessFunc);
    }

    public getValue(): Journey[] {
        return this.value;
    }

    public getTotalDistance(): number {
        if (this.totalDistance !== 0) {
            return this.totalDistance;
        }

        if (this.value.some(journey => !journey.getIsPossible())) {
            return -1;
        }

        this.totalDistance = this.value
            .map(journey => journey.getDistance())
            .reduce((total, current) => total + current);

        return this.totalDistance;
    }
}
