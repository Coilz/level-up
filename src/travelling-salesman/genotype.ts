import { Chromosome } from "../genetic-algorithm/chromosome";
import { MateFunc, MutateFunc, IChromosome } from "../genetic-algorithm/genetics";
import { Place } from "./place";

export class GenoTypeBuilder {
    private crossOver(valueA: Place[], valueB: Place[], index: number): Place[] {
        // order crossover operator (OX1): A portion of one parent is mapped to a portion of the other parent.
        // From the replaced portion on, the rest is filled up by the remaining genes,
        // where already present genes are omitted and the order is preserved.
        let left = valueA.slice(0, index);
        let right = valueB.filter(place =>
            left.findIndex(leftPlace =>
                leftPlace.getName() === place.getName()) === -1);

        return left.concat(right);
    }

    private readonly mate: MateFunc = (a: IChromosome, b: IChromosome): IChromosome[] => {
        let parentA = (a as Genotype).getValue();
        let parentB = (b as Genotype).getValue();

        let index = Math.floor(Math.random() * parentA.length);
        let childA = this.build(this.crossOver(parentA, parentB, index));
        let childB = this.build(this.crossOver(parentB, parentA, index));

        let children: Genotype[] = [];
        children.push(childA);
        children.push(childB);

        return children;
    }

    private readonly mutate: MutateFunc = (chromosome: IChromosome): void => {
        if (Math.random() > 0.07) {
            return;
        }

        let value = (chromosome as Genotype).getValue();
        let i = Math.floor(Math.random() * value.length);
        let j = Math.floor(Math.random() * value.length);

        let elementI = value[i];
        let elementJ = value[j];

        // swap two elements
        value.splice(i, 1, elementJ);
        value.splice(j, 1, elementI);
    }

    public build(value: Place[]): Genotype {
        return new Genotype(value, this.mate, this.mutate);
    }
}

export class Genotype extends Chromosome {
    constructor(private value: Place[], mate: MateFunc, mutate: MutateFunc) {
        super(mate, mutate);
    }

    public getValue(): Place[] {
        return this.value;
    }
}
