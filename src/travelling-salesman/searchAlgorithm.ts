import { Phenotype, PhenotypeBuilder } from "./phenotype";
import { Algorithm, TerminateFunc } from "../genetic-algorithm/algorithm";
import { IChromosome } from "../genetic-algorithm/genetics";
import { Genotype } from "./genotype";
import { DecodeFunc } from "../genetic-algorithm/candidate";
import { RoulettewheelByRank } from "../genetic-algorithm/roulettewheelSelectionByRank";
import { PopulationBuilder } from "../genetic-algorithm/population";
import { Journey } from "./journey";

export class SearchAlgorithm {
    private algorithm: Algorithm<Phenotype>;
    private phenotypeBuilder: PhenotypeBuilder;
    private distanceCounter = 0;
    private previousDistance = 0;

    private decode: DecodeFunc<Phenotype> = (chromosome: IChromosome) => {
        let genValue = (chromosome as Genotype).getValue();

        let journeys: Journey[] = [];
        for (var i = 0; i < genValue.length; i++) {
            var start = genValue[i];
            var end = i === genValue.length - 1 ?
                genValue[0] :
                genValue[i + 1];

            let index = this.possibleJourneys.findIndex(possibleJourney =>
                possibleJourney.getStart().getName() === start.getName() &&
                possibleJourney.getEnd().getName() === end.getName());

            if (index >= 0) {
                journeys.push(this.possibleJourneys[index]);
            } else {
                journeys.push(Journey.getImpossibleJourney(start, end));
            }
        }

        return this.phenotypeBuilder.build(journeys);
    }

    private terminate: TerminateFunc<Phenotype> = (phenoType: Phenotype): boolean => {
        let bestValue = phenoType.getValue();
        let totalDistance = bestValue
            .map(journey => journey.getDistance())
            .reduce((total, current) => total + current);

        if (totalDistance === this.previousDistance) {
            this.distanceCounter++;
        } else {
            this.distanceCounter = 0;
            this.previousDistance = totalDistance;
        }
        console.log(totalDistance);

        return this.distanceCounter === 15;
    }

    constructor(private possibleJourneys: Journey[], chromosomes: IChromosome[]) {
        let selection = new RoulettewheelByRank<Phenotype>();

        this.phenotypeBuilder = new PhenotypeBuilder();
        let populationBuilder = new PopulationBuilder(selection.getSelection(), this.decode);
        this.algorithm = new Algorithm<Phenotype>(chromosomes, populationBuilder, this.terminate);
    }

    public Run(): Journey[] {
        return this.algorithm.Run().getValue();
    }
}
