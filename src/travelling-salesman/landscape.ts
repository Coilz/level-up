import { Place } from "./place";
import { Journey } from "./journey";

export class Landscape {
    public static getPlaces(): Place[] {
        return [
            new Place("A", 100, 10, 0),
            new Place("B", 20, 30, 0),
            new Place("C", 120, 60, 0),
            new Place("D", 240, 60, 4),
            new Place("E", 190, 80, 5),
            new Place("F", 110, 100, 0),
            new Place("G", 220, 160, 0),
            new Place("H", 120, 180, 0),
            new Place("I", 210, 180, 0),
            new Place("J", 10, 210, 0),
            new Place("K", 180, 240, 0),
            new Place("L", 80, 250, 0),
            new Place("M", 160, 260, 0),
            new Place("N", 240, 270, 0),
            new Place("O", 120, 300, 10),
            new Place("P", 250, 330, 0),
            new Place("Q", 70, 340, 0),
            new Place("R", 230, 350, 3),
            new Place("S", 140, 360, 20),
            new Place("T", 30, 380, 10)
        ];
    }

    public static getJourneys(): Journey[] {
        let journeys: Journey[] = [];
        let places = Landscape.getPlaces();
        places.forEach(i => {
            places.forEach(j => {
                if (i.getName() !== j.getName()) {
                    journeys.push(new Journey(i, j));
                }
            });
        });

        return journeys;
    }
}
