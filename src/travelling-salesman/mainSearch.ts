import { SearchAlgorithm } from "./searchAlgorithm";
import { Genotype, GenoTypeBuilder } from "./genotype";
import { Place } from "./place";
import { Landscape } from "./landscape";

export class MainSearch {
    private algorithm: SearchAlgorithm;
    constructor(populationSize: number) {
        let places = Landscape.getPlaces();
        let possibleJourneys = Landscape.getJourneys();

        let randomGenotypes = MainSearch.generateInitialPopulation(populationSize, places);
        this.algorithm = new SearchAlgorithm(possibleJourneys, randomGenotypes);
    }

    public Run(): string {
        let journeys = this.algorithm.Run();
        return journeys
            .map(journey => "{" +
                    journey.getStart().getName() + "=>" + journey.getEnd().getName() +
                    ":" + journey.getDistance() + "}\n")
            .reduce((total, current) => total + current);
    }

    private static generateInitialPopulation(count: number, places: Place[]): Genotype[] {
        let genotypes: Genotype[] = [];
        let builder = new GenoTypeBuilder();

        for (var i = 0; i < count; i++) {
            var permutation = MainSearch.generatePermutation(places);
            let genotype = builder.build(permutation);
            genotypes.push(genotype);
        }

        return genotypes;
    }

    private static generatePermutation(places: Place[]): Place[] {
        let temp = places.slice(0);
        let result: Place[] = [];

        while (temp.length > 0) {
            let index = Math.floor(Math.random() * temp.length);
            let item = temp[index];
            temp.splice(index, 1);
            result.push(item);
        }

        return result;
    }
}
