import { Place } from "./place";

export class Journey {

    public static getImpossibleJourney(start: Place, end: Place): Journey {
        return new Journey(start, end, 0, 0, false);
    }

    private distance = 0;

    constructor(
        private start: Place,
        private end: Place,
        private cost: number = 0,
        private time: number = 0,
        private isPossible: boolean = true) {
    }

    public getStart(): Place {
        return this.start;
    }
    public getEnd(): Place {
        return this.end;
    }
    public getDistance(): number {
        if (this.distance === 0) {
            this.distance = this.isPossible ?
                this.start.distance(this.end) :
                this.distance = Number.MAX_SAFE_INTEGER;
        }

        return this.distance;
    }
    public getCost(): number {
        return this.cost;
    }
    public getTime(): number {
        return this.time;
    }

    public getIsPossible() : boolean {
        return this.isPossible;
    }
}
