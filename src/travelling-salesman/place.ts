export class Place {
    constructor(
        private name: string,
        private x: number,
        private y: number,
        private z: number) {
    }

    public getName(): string {
        return this.name;
    }

    public distance(other: Place): number {
        return Math.sqrt(
            Math.pow(this.x - other.x, 2) +
            Math.pow(this.y - other.y, 2) +
            Math.pow(this.z - other.z, 2));
    }
}
