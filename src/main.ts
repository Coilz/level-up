import { MainSearch as StringSearch } from "./string-search/mainSearch";
import { MainSearch as RouteSearch } from "./travelling-salesman/mainSearch";

export class Main {
  public static findString (targetString: string) : void {
    let mySearch = new StringSearch(targetString, 8 * 128);
    let result = mySearch.Run();

    console.log(result);
  }

  public static findRoute () : void {
    let mySearch = new RouteSearch(8 * 128);
    let result = mySearch.Run();

    console.log(result);
  }
}

Main.findString("Optimization refers to finding the values of inputs in such a way that we get the 'best' output values.");
//Main.findRoute();
