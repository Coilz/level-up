import { IPhenotype } from "./phenotype";
import { IChromosome } from "./genetics";
import { SelectionFunc } from "./algorithm";

export interface ISelectionProvider<TPhenotype extends IPhenotype> {
    getSelection() : SelectionFunc<TPhenotype>;
}

export interface ISelection {
    selectParent() : IChromosome;
}
