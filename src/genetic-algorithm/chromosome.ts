import { IChromosome, MateFunc, MutateFunc } from "./genetics";

export class Chromosome implements IChromosome {

    constructor(private mateFunction: MateFunc, private mutateFunction: MutateFunc) {
    }

    public mutate(): void {
        this.mutateFunction(this);
    }

    public mate(chromosome: IChromosome): IChromosome[] {
        return this.mateFunction(this, chromosome);
    }
}
