import { IChromosome } from "./genetics";
import { IPopulation, PopulationBuilder } from "./population";
import { Candidate } from "./candidate";
import { IPhenotype } from "./phenotype";

export interface FitnessFunc {
    (phenotype: IPhenotype): number;
}

export interface SelectionFunc<TPhenotype extends IPhenotype> {
    (canditates: Candidate<TPhenotype>[]): IChromosome;
}

export interface TerminateFunc<TPhenotype extends IPhenotype> {
    (phenotype: TPhenotype): boolean;
}

export class Algorithm<TPhenotype extends IPhenotype> {
    private population: IPopulation<TPhenotype>;

    constructor(
        chromosomes: IChromosome[],
        private populationBuilder: PopulationBuilder<TPhenotype>,
        private terminate: TerminateFunc<TPhenotype>) {
        this.population = populationBuilder.build(chromosomes);
    }

    public Run(): TPhenotype {
        let bestCandidate = this.population.getBestCandidate().getPhenotype();
        let cycleCount = 0;
        while (!this.terminate(bestCandidate)) {
            this.ExecuteCycle();
            cycleCount++;
            bestCandidate = this.population.getBestCandidate().getPhenotype();
        }

        console.log("Number of generations: " + cycleCount);

        return bestCandidate;
    }

    private ExecuteCycle(): void {
        this.population.ExecuteCycle();

        let newGenotypes: IChromosome[] = [];
        while (newGenotypes.length < this.population.getSize()) {
            let parentOne = this.population.selectParent();
            let parentTwo = this.population.selectParent();

            let children = parentOne.mate(parentTwo);
            children.forEach(child => {
                child.mutate();
                newGenotypes.push(child);
            });
        }

        this.population = this.populationBuilder.build(newGenotypes);
    }
}
