import { IPhenotype } from "./phenotype";
import { ISelectionProvider } from "./selection";
import { Candidate } from "./candidate";
import { SelectionFunc } from "./algorithm";

export class RoulettewheelByRank<TPhenotype extends IPhenotype> implements ISelectionProvider<TPhenotype> {
    private selectionFunc: SelectionFunc<TPhenotype> = (canditates: Candidate<TPhenotype>[]) => {
        let populationSize = canditates.length;
        let maxPoolIndex = this.calculateMaxPoolIndex(populationSize);
        let randomPoolIndex = 1 + Math.floor(Math.random() * maxPoolIndex);

        for (var i = 0; i < populationSize; i++) {
            let nextMaxPoolIndex = this.calculateMaxPoolIndex(populationSize - i - 1);

            if (randomPoolIndex > nextMaxPoolIndex) {
                return canditates[i].getGenoType();
            }
        }

        throw new Error("rouletteWheelSelectionByRank didn't select a score: " + randomPoolIndex);
    }

    private calculateMaxPoolIndex(rank: number): number {
        return rank * (rank + 1) / 2;
    }

    public getSelection(): SelectionFunc<TPhenotype> {
        return this.selectionFunc;
    }
}
