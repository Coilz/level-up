export interface MateFunc {
    (a : IChromosome, b : IChromosome) : IChromosome[];
}

export interface MutateFunc {
    (chromosome : IChromosome) : void;
}

export interface IChromosome {
    mutate() : void;
    mate(chromosome : IChromosome) : IChromosome[];
}
