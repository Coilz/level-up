import { IChromosome } from "./genetics";
import { IPhenotype } from "./phenotype";

export interface DecodeFunc<TPhenotype extends IPhenotype> {
    (chromosome : IChromosome) : TPhenotype;
}

export class Candidate<TPhenotype extends IPhenotype> {
    constructor(private phenotype: TPhenotype, private genotype: IChromosome) {
    }

    public getPhenotype(): TPhenotype {
        return this.phenotype;
    }

    public getGenoType(): IChromosome {
        return this.genotype;
    }

    public compare(other: Candidate<TPhenotype>): number {
        if (other === null) {
            return 1;
        }

        return this.phenotype.compare(other.phenotype);
    }
}
