import { IChromosome } from "./genetics";
import { Candidate, DecodeFunc } from "./candidate";
import { IPhenotype } from "./phenotype";
import { ISelection } from "./selection";
import { SelectionFunc } from "./algorithm";

export interface IPopulation<TPhenotype extends IPhenotype> extends ISelection {
    getBestCandidate(): Candidate<TPhenotype>;
    getSize(): number;
    ExecuteCycle(): void;
}

export class PopulationBuilder<TPhenotype extends IPhenotype> {
    constructor(
        private selection: SelectionFunc<TPhenotype>,
        private decode: DecodeFunc<TPhenotype>) {
    }

    public build(chromosomes: IChromosome[]): IPopulation<TPhenotype> {
        return new Population(this.selection, this.decode, chromosomes);
    }
}

export class Population<TPhenotype extends IPhenotype> implements IPopulation<TPhenotype> {
    private readonly canditates: Candidate<TPhenotype>[] = [];

    constructor(
        private selection: SelectionFunc<TPhenotype>,
        decode: DecodeFunc<TPhenotype>,
        chromosomes: IChromosome[]) {
        chromosomes.forEach(chromosome => {
            let canditate = new Candidate(decode(chromosome), chromosome);
            this.canditates.push(canditate);
        });
    }

    public getBestCandidate(): Candidate<TPhenotype> {
        return this.canditates[0];
    }

    public getSize(): number {
        return this.canditates.length;
    }

    public selectParent(): IChromosome {
        return this.selection(this.canditates);
    }

    public ExecuteCycle(): void {
        this.canditates.forEach(canditate => {
            canditate.getPhenotype().ExecuteCycle();
        });

        this.canditates.sort(this.canditateComparer).reverse();
    }

    private canditateComparer(a: Candidate<TPhenotype>, b: Candidate<TPhenotype>): number {
        if (a === null) {
            if (b === null) {
                return 0;
            }
            return 1;
        }

        if (b === null) {
            return -1;
        }

        return a.getPhenotype().compare(b.getPhenotype());
    }
}
