import { FitnessFunc } from "./algorithm";

export interface IPhenotype {
    getFitness() : number;
    ExecuteCycle(): void;
    compare(other: IPhenotype): number;
}

export class Phenotype implements IPhenotype {
    private fitness: number;

    constructor(private fitnessFunc: FitnessFunc) {
        this.fitnessFunc = fitnessFunc;
    }

    public ExecuteCycle(): void {
        this.fitness = this.fitnessFunc(this);
    }

    public getFitness(): number {
        return this.fitness;
    }

    public compare(other: IPhenotype): number {
        if (other === null) {
            return 1;
        }

        return this.fitness === other.getFitness() ?
            0 :
            this.fitness > other.getFitness() ?
                1 :
                -1;
    }
}
