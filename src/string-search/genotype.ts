import { MateFunc, MutateFunc } from "../genetic-algorithm/genetics";
import { Chromosome } from "../genetic-algorithm/chromosome";

export class Genotype extends Chromosome {
    constructor(private value: string[], mate: MateFunc, mutate: MutateFunc) {
        super(mate, mutate);
    }

    public getValue(): string[] {
        return this.value;
    }
}
