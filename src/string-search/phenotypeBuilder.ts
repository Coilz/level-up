import { FitnessFunc } from "../genetic-algorithm/algorithm";
import { IPhenotype } from "../genetic-algorithm/phenotype";
import { Phenotype } from "./phenotype";

export class PhenotypeBuilder {
    private readonly fitness: FitnessFunc = (phenotype: IPhenotype) => {
        let currentValue = (phenotype as Phenotype).getValue();

        if (this.desiredValue.length !== currentValue.length) {
            return -1;
        }

        let fitness = 0;
        for (var i = 0; i < currentValue.length; i++) {
            if (currentValue.charAt(i) === this.desiredValue.charAt(i)) {
                fitness++;
            }
        }

        return fitness;
    }

    constructor(private desiredValue: string) {
    }

    public build(value: string): Phenotype {
        return new Phenotype(value, this.fitness);
    }
}
