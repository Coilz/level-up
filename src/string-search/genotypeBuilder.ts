import { MateFunc, MutateFunc, IChromosome } from "../genetic-algorithm/genetics";
import { StringGenerator } from "./stringGenerator";
import { Genotype } from "./genotype";

export class GenoTypeBuilder {
    private crossOver(valueA: string[], valueB: string[], index: number): string[] {
        let left = valueA.slice(0, index);
        let right = valueB.slice(index);

        return left.concat(right);
    }

    private readonly mate: MateFunc = (a: IChromosome, b: IChromosome): IChromosome[] => {
        let parentA = (a as Genotype).getValue();
        let parentB = (b as Genotype).getValue();

        let index = Math.floor(Math.random() * parentA.length);
        let childA = this.build(this.crossOver(parentA, parentB, index));
        let childB = this.build(this.crossOver(parentB, parentA, index));

        let children: Genotype[] = [];
        children.push(childA);
        children.push(childB);

        return children;
    }

    private readonly mutate: MutateFunc = (chromosome: IChromosome): void => {
        if (Math.random() > 0.35) {
            return;
        }

        let value = (chromosome as Genotype).getValue();
        let index = Math.floor(Math.random() * value.length);

        value.splice(index, 1, StringGenerator.generateCharacter()); // Can also be the character next to the current
        // value.splice(index, 1, StringGenerator.mutateCharacter(value[index])); // Can also be the character next to the current
    }

    public build(value: string[]): Genotype {
        return new Genotype(value, this.mate, this.mutate);
    }
}
