import { FitnessFunc } from "../genetic-algorithm/algorithm";
import { Phenotype as BasePhenotype } from "../genetic-algorithm/phenotype";

export class Phenotype extends BasePhenotype {
    constructor(private value: string, fitnessFunc: FitnessFunc) {
        super(fitnessFunc);
    }

    public getValue(): string {
        return this.value;
    }
}
