export class StringGenerator {
    public static generateCharacterArrays(count: number, length: number): string[][] {
        let result: string[][] = [];

        for (let i = 0; i < count; i++) {
            let element = StringGenerator.generateCharacterArray(length);
            result.push(element);
        }

        return result;
    }

    public static generateCharacterArray(length: number): string[] {
        let result: string[] = [];

        for (let i = 0; i < length; i++) {
            let element = StringGenerator.generateCharacter();
            result.push(element);
        }

        return result;
    }

    public static generateCharacter(): string {
        let charIndex: number = 32 + Math.floor(Math.random() * (126 - 32));
        return String.fromCharCode(charIndex);
    }


    public static mutateCharacter(value: string): string {
        let charCode = value.charCodeAt(0);
        let change = Math.random() < 0.5 ? -1 : 1;

        if (charCode + change < 32 || charCode + change > 126) {
            change = 0;
        }

        return String.fromCharCode(charCode + change);
    }
}
