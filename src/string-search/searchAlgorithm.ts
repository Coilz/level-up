import { Phenotype } from "./phenotype";
import { PhenotypeBuilder } from "./phenotypeBuilder";
import { Algorithm, TerminateFunc } from "../genetic-algorithm/algorithm";
import { IChromosome } from "../genetic-algorithm/genetics";
import { Genotype } from "./genotype";
import { DecodeFunc } from "../genetic-algorithm/candidate";
import { RoulettewheelByRank } from "../genetic-algorithm/roulettewheelSelectionByRank";
import { PopulationBuilder } from "../genetic-algorithm/population";

export class SearchAlgorithm {
    private algorithm: Algorithm<Phenotype>;
    private phenotypeBuilder: PhenotypeBuilder;

    private decode: DecodeFunc<Phenotype> = (chromosome: IChromosome) => {
        let genValue = (chromosome as Genotype).getValue();
        let value = genValue.join("");
        return this.phenotypeBuilder.build(value);
    }

    private terminate: TerminateFunc<Phenotype> = (phenoType: Phenotype): boolean => {
        let bestValue = phenoType.getValue();
        console.log(bestValue);

        return bestValue === this.desiredValue;
    }

    constructor(private desiredValue: string, chromosomes: IChromosome[]) {
        let selection = new RoulettewheelByRank<Phenotype>();

        this.phenotypeBuilder = new PhenotypeBuilder(desiredValue);
        let populationBuilder = new PopulationBuilder(selection.getSelection(), this.decode);
        this.algorithm = new Algorithm<Phenotype>(chromosomes, populationBuilder, this.terminate);
    }

    public Run(): string {
        return this.algorithm.Run().getValue();
    }
}
