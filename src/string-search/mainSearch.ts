import { SearchAlgorithm } from "./searchAlgorithm";
import { Genotype } from "./genotype";
import { GenoTypeBuilder } from "./genotypeBuilder";
import { StringGenerator } from "./stringGenerator";

export class MainSearch {
    private algorithm: SearchAlgorithm;
    constructor(desiredValue: string, populationSize: number) {
        let randomGenotypes = MainSearch.generateInitialPopulation(populationSize, desiredValue.length);
        this.algorithm = new SearchAlgorithm(desiredValue, randomGenotypes);
    }

    public Run(): string {
        return this.algorithm.Run();
    }

    private static generateInitialPopulation(count: number, stringLength: number): Genotype[] {
        let strings = StringGenerator.generateCharacterArrays(count, stringLength);

        let builder = new GenoTypeBuilder();
        return strings.map(element =>
            builder.build(element));
    }
}
