let gulp = require("gulp");
let ts = require("gulp-typescript");
let shell = require('gulp-shell');

let tsProject = ts.createProject("tsconfig.json");

let build = () =>
  tsProject.src()
    .pipe(tsProject())
    .js
    .pipe(gulp.dest("app"));

let run = shell.task([
  'node app/main.js'
]);

gulp.task('run', run);

gulp.task('watch', ['build'], () => {
  gulp.watch(tsProject.config.include, ['build'])
});

gulp.task('build', build);

gulp.task('buildrun', ['build'], run);
gulp.task('default', ['buildrun']);
